import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class TransactionReader {
    public static void main(String[] args) {

        if (args.length == 0){
            System.out.println("No input file provided! Aborting!");
            return;
        }
        String inputFile = args[0];
        System.out.println("args[0]");
        TransactionReader reader = new TransactionReader();
        reader.start(inputFile);
    }

    private void start(String inputFile) {
        String line = null;
        Path path = Paths.get(inputFile);
        int lineNumber = 0;
        BufferedReader br;
        try {
            br = Files.newBufferedReader(path, Charset.defaultCharset());
        } catch (IOException e) {
            e.printStackTrace();
        }
        while ((line = br.readLine()) != null) {
            lineNumber++;
            if (lineNumber == 1) continue; //skips first header line
            String[] elements = line.split("\t"); //split on tabs
            String name = elements[0];
            int age = Integer.parseInt(elements[1]); //convert to int
            String function = elements[2];
//convert to double
            double salary = Double.parseDouble(elements[3]) * 1000;
            System.out.println("name=" + name
                    + "; age=" + age
                    + "; function=" + function
                    + "; salary=" + salary);
        }

    }
}
